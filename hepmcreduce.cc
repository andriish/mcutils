// -*- C++ -*-
//
// This file is part of MCUtils -- https://bitbucket.org/andybuckley/mcutils
// Copyright (C) 2013-2014 Andy Buckley <andy.buckley@cern.ch>
//
// Embedding of MCUtils code in other projects is permitted provided this
// notice is retained and the MCUtils namespace and include path are changed.
//

#include "MCUtils/HepMCUtils.h"
#include "HepMC/IO_GenEvent.h"
//#include "MCUtils/Clustering.h" //< Unnecessary, but forces compile-time testing of that header!
using namespace std;

/// @file Demo of filtering a HepMC record
/// @author Andy Buckley <andy.buckley@cern.ch>
//
// NOTE: Requires HepMC. Compile with e.g.
//  g++ -o hepmcreduce hepmcreduce.cc -Iinclude -I/path/to/hep/installs/include -L/path/to/hep/installs/lib -lHepMC


// A classifier function for GenParticles we want to remove
bool find_bad_particles(const HepMC::GenParticle* p) {
  // Remove "null" particles used for HEPEVT padding
  if (p->pdg_id() == 0) return true;
  // Always keep EW particles, BSM particles, and heavy flavour partons & hadrons
  /// @todo Also keep particles directly connected to EW & BSM objects?
  /// @todo Intermediate replica removal?
  if (MCUtils::isResonance(p)) return false;
  if (MCUtils::isBSM(p)) return false;
  if (MCUtils::isHeavyFlavour(p)) return false;
  // Kill (simple) loops
  if (p->production_vertex() == p->end_vertex() && p->end_vertex() != NULL) return true;
  // Remove G4-unsafe particles in decay chains
  if ((MCUtils::fromDecay(p) && !MCUtils::isTransportable(p))) return true;
  // Remove unstable hadrons from decay chains not containing heavy flavour or taus
  if (MCUtils::isDecayed(p) && !(MCUtils::isParton(p) || MCUtils::fromTauOrHFDecay(p))) return true;
  // Remove partons (other than those already preserved)
  if (MCUtils::isParton(p)) return true;
  // Remove non-standard particles
  if (MCUtils::isGenSpecific(p)) return true;
  if (MCUtils::isDiquark(p)) return true;
  //if (MCUtils::hasNonStandardStatus(p)) return true;
  // Apply cuts on |eta| and pT (to be used only for thinning G4 particles)
  if (p->barcode() > 200000 && (!MCUtils::InEtaRange(-5.0, 5.0)(p) || !MCUtils::PtGtr(0.3)(p))) return true;
  // Remove stable particles (to remove clutter for debug visualisation only!!!)
  //if (MCUtils::isStable(p)) return true;
  //if (MCUtils::isStable(p) && (p->pdg_id() == 111 || abs(p->pdg_id()) == 211)) return true;
  //if (MCUtils::isStable(p) && p->pdg_id() == 22) return true;
  return false;
}


int main(int argc, char** argv) {
  // Configure input event file from command line
  string infile = "in.hepmc";
  if (argc > 1) infile = argv[1];
  HepMC::IO_GenEvent in(infile, ios::in);

  // Configure output event file from command line
  string outfile = infile.substr(0, infile.rfind(".")) + "-reduced.hepmc";
  if (argc > 2) outfile = argv[2];
  HepMC::IO_GenEvent out(outfile, ios::out);

  // Event loop
  int nevt = 0;
  HepMC::GenEvent* ge = in.read_next_event();
  while (ge) {
    nevt += 1;

    // Event properties before filtering
    const int particles_size_orig = ge->particles_size();
    const int vertices_size_orig = ge->vertices_size();
    #ifdef CHECKING
    const int num_orphan_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::isDisconnected).size();
    const int num_noparent_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::hasNoParents).size();
    const int num_nochild_vtxs_orig = MCUtils::const_vertices_match(ge, MCUtils::hasNoChildren).size();
    #endif

    // Consistently remove the unwanted particles from the event
    MCUtils::reduce(ge, find_bad_particles);

    // Event properties after filtering
    const int particles_size_filt = ge->particles_size();
    const int vertices_size_filt = ge->vertices_size();
    #ifdef CHECKING
    const int num_orphan_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::isDisconnected).size();
    const int num_noparent_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::hasNoParents).size();
    const int num_nochild_vtxs_filt = MCUtils::const_vertices_match(ge, MCUtils::hasNoChildren).size();
    #endif

    // Write out the change in the number of particles, etc.
    cout << "#" << nevt << ": "
         << particles_size_orig << ", " << vertices_size_orig << " -> "
         << particles_size_filt << ", " << vertices_size_filt << endl;
    #ifdef CHECKING
    if (num_orphan_vtxs_filt != num_orphan_vtxs_orig)
      cerr << "WARNING! Change in num orphaned vertices: "
           << num_orphan_vtxs_orig << " -> " << num_orphan_vtxs_filt << endl;
    if (num_noparent_vtxs_filt != num_noparent_vtxs_orig)
      cerr << "WARNING! Change in num no-parent vertices: "
           << num_noparent_vtxs_orig << " -> " << num_noparent_vtxs_filt << endl;
    if (num_nochild_vtxs_filt != num_nochild_vtxs_orig)
      cerr << "WARNING! Change in num no-child vertices: "
           << num_nochild_vtxs_orig << " -> " << num_nochild_vtxs_filt << endl;
    #endif

    // Write out reduced event
    out << ge;
    delete ge;
    in >> ge;
  }
  return 0;
}
