#include "MCUtils/PIDUtils.h"
using namespace MCUtils;
#include <iostream>
using namespace std;


int main(int argc, char** argv) {
  if (argc < 2) return 0;
  const int pid = atoi(argv[1]);

  cout << "PID = " << pid << endl;
  const bool valid = PID::isValid(pid);
  cout << (valid ? "VALID" : "INVALID") << endl;
  if (!valid) exit(1);

  if (PID::isNucleus(pid)) cout << "Nucleus" << endl;
  if (PID::isBSM(pid)) cout << "BSM" << endl;
  if (PID::isLepton(pid)) cout << "Lepton" << endl;
  if (PID::isPhoton(pid)) cout << "Photon" << endl;
  if (PID::isHadron(pid)) cout << "Hadron" << endl;
  if (PID::isParton(pid)) cout << "Parton" << endl;
  if (PID::isQuark(pid)) cout << "Quark" << endl;
  if (PID::isGluon(pid)) cout << "Gluon" << endl;
  if (PID::isDiquark(pid)) cout << "Diquark" << endl;
  if (PID::isMeson(pid)) cout << "Meson" << endl;
  if (PID::isBaryon(pid)) cout << "Baryon" << endl;
  if (PID::hasCharm(pid)) cout << "Has charm" << endl;
  if (PID::hasBottom(pid)) cout << "Has bottom" << endl;
  cout << "2J+1 = " << PID::jSpin(pid) << ", "
       << "2S+1 = " << PID::sSpin(pid) << ", "
       << "2L+1 = " << PID::lSpin(pid) << endl;
  cout << "Q = " << PID::threeCharge(pid) << "/3 = " << PID::charge(pid) << endl;
  cout << "Coloured = " << boolalpha << PID::isStrongInteracting(pid) << endl;
  return 0;
}
